# Check run
# =========
# SPC/E water -- NIST configuration 1

# Global parameters
num_steps       500000
timestep        82.642 #0.000001
temperature     298

# Box
# ---
num_pbc  2

# Thermostat
# ----------
thermostat
   relaxation_time 41341
   max_iteration   50
   tolerance       1.0e-17
   chain_length    5

# Species definition
# ---------------

species

  species_type
    name        O
    count      2160
    charge  point -0.8476
    mobile True
    mass   15.9994

  species_type
    name H1
    count     2160
    charge point 0.4238
    mobile True
    mass 1.008

  species_type
    name H2
    count     2160
    charge point 0.4238
    mobile True
    mass 1.008

  species_type
    name Au1                         # name of the species
    count 1620                      # number of species 
    mass 196.966553                 # mass in amu
    mobile False
    charge gaussian 0.6303 0.0 # wall charge eta

  species_type
    name Au2                         # name of the species
    count 1620                      # number of species 
    mass 196.966553                      # mass in amu
    mobile False
    charge gaussian 0.6303 0.0 # wall charge eta

molecules

  molecule_type
    name SPC
    count    2160
    sites O H1 H2

    constraint O   H1   1.88972
    constraint O   H2   1.88972
    constraint H1  H2   3.08587

    constraints_algorithm rattle 1.0e-7 100

# Electrode species definition
# ----------------------
electrodes

  electrode_type
    name       Au1             # name of electrode
    species    Au1             # name of the species
    potential  -0.0           # fixed potential 1V = -0.03674932379085202 ua
#    thomas_fermi_length 1.09604   #  0.58 A
#    voronoi_volume 113.74173222606741
#   piston     3.443861840114266e-09   +1.0   # 1 atm

  electrode_type
    name       Au2             # name of the species
    species    Au2             # name of the species
    potential  +0.0           # fixed potential 1V = +0.03674932379085202 ua
#    thomas_fermi_length 1.09604   #  0.58 A
#    voronoi_volume 113.74173222606741
#   piston     3.443861840114266e-09   -1.0   # 1 atm

  electrode_charges matrix_inversion
  charge_neutrality true

interactions
  coulomb
    coulomb_rtol       2.0e-5
    coulomb_rcut       22.68
    coulomb_ktol       1.0e-6

  lennard-jones
    lj_rcut 28.3459
    lj_pair O  O  0.6502 3.166
    lj_pair Au1 Au1 22.13336 2.951
    lj_pair Au2 Au2 22.13336 2.951
    lj_rule Lorentz_Berthelot
    #lj_pair O  Au1 3.61 3.18 
    #lj_pair O  Au2 3.61 3.18

output
  default 0
  step 100
  charges 1000
  total_charges 50
  lammps 1000
  xyz    1000
  restart 1000 alternate
  density 1000 100 -13.05 148.65
