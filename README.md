# Serva_JChemPhys_155_044703_2021

Contains input files and data used to generate the figures of the article:

Effect of the metallicity on the capacitance of gold -- aqueous sodium chloride interfaces

Alessandra Serva, Laura Scalfi, Benjamin Rotenberg, and Mathieu Salanne,
*J. Chem. Phys.*, 155, 044703, 2021

https://doi.org/10.1063/5.0060316 ([see here for a preprint](https://arxiv.org/abs/2106.07232))

The folder *InputFiles* contains 2 input files for each system for MetalWalls, which is available [here](https://gitlab.com/ampere2/metalwalls)

The folders *FigureN* where N goes from 1 to 8 contain files with the data used to plot the Figures of the paper 
